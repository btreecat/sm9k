#ifndef DRAW_H
#define DRAW_H

#include <U8g2lib.h>
#include <globals.h>


extern U8G2_SSD1306_128X64_NONAME_F_SW_I2C U8G2_DISPLAY;

void draw_IP(const char *ip, char const *ssid);
void draw_boot();
void draw_temps(float temp_main, float temp_secondary, int probe_id);
void draw_string(char *msg);

#endif