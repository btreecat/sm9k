#ifndef METHODS_H
#define METHODS_H

enum HTTP_METHODS
{
    GET_METHOD,
    POST_METHOD,
    UNKNOWN_METHOD,
};

#endif