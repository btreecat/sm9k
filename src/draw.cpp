#include <draw.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C U8G2_DISPLAY(U8G2_R0, 4, 5);


void draw_string(char *msg)
{
    U8G2_DISPLAY.clearBuffer();
    U8G2_DISPLAY.setFont(u8g2_font_ncenB14_tr);
    U8G2_DISPLAY.drawStr(0, 20, msg);
    U8G2_DISPLAY.sendBuffer();
}

void draw_temps(float temp_C, float temp_F, int probe_id)
{
    U8G2_DISPLAY.clearBuffer();
    char temp_main[10];
    char temp_second[10];
    char header[10];

    sprintf(header, "%d", probe_id);

    if (TEMP_UNIT_F)
    {
        //main temp is in F seondary in C
        sprintf(temp_main, "%.1f F", temp_F);
        sprintf(temp_second, "%.1f C", temp_C);
    }
    else
    {
        //main temp is in C secondary in F
        sprintf(temp_main, "%.1f C", temp_C);
        sprintf(temp_second, "%.1f F", temp_F);
    }

    U8G2_DISPLAY.setFont(u8g2_font_tenstamps_mn);
    U8G2_DISPLAY.drawStr(100, 20, header);
    U8G2_DISPLAY.setFont(u8g2_font_ncenB18_tr);
    U8G2_DISPLAY.drawStr(0, 30, temp_main);
    U8G2_DISPLAY.setFont(u8g2_font_ncenB10_tr);
    U8G2_DISPLAY.drawStr(70, 60, temp_second);
    U8G2_DISPLAY.sendBuffer();
}

void draw_IP(const char *ip, char const *ssid)
{
    char ap[20];
    U8G2_DISPLAY.clearBuffer();
    U8G2_DISPLAY.setFont(u8g2_font_ncenB12_tr);
    sprintf(ap, "%s", ssid);
    U8G2_DISPLAY.drawStr(0, 20, ap);
    U8G2_DISPLAY.drawStr(0, 50, ip);
    U8G2_DISPLAY.sendBuffer();
}

void draw_boot()
{
    U8G2_DISPLAY.clearBuffer();
    U8G2_DISPLAY.setFont(u8g2_font_ncenB12_tr);
    U8G2_DISPLAY.drawStr(40, 20, "Smoke");
    U8G2_DISPLAY.drawStr(20, 40, "Master 9K");
    U8G2_DISPLAY.setFont(u8g2_font_ncenB08_tr);
    U8G2_DISPLAY.drawStr(42, 58, "booting...");
    U8G2_DISPLAY.sendBuffer();
}