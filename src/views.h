#ifndef VIEWS_H
#define VIEWS_H

#include <templates.h>
#include <Arduino.h>
#include <WebServer.h>
#include <globals.h>


String view_root(bool temp_unit_F);
String view_cfg(bool temp_unit_F, String ssid, String pass);
String view_api();
String view_404(String uri, HTTPMethod method);
String view_alarms(bool temp_unit_F, Alarm *alarm_set);

#endif