#ifndef ROUTES_H
#define ROUTES_H

#include <Arduino.h>
#include <globals.h>
#include <templates.h>
#include <WebServer.h>
#include <config.h>
#include <views.h>
#include <utils.h>


extern WebServer http_server;

void register_routes();

enum HTTP_STATUS {
    HTTP_OK = 200,
    HTTP_NOT_FOUND = 404  
};

#endif