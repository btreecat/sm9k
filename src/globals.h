#ifndef GLOBALS_H
#define GLOBALS_H

#include <esp_adc_cal.h>
#include <config.h>
#include <reading.h>
#include <alarm.h>
#include <Preferences.h>
#include <station.h>


//Globals
extern bool TEMP_UNIT_F;
extern Preferences NV_STORAGE;
extern Reading PROBE_READINGS[];
extern Alarm ALARM_SETTINGS[];
extern WiFi_Station WIFI_STATIONS[];

#endif