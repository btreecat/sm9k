#ifndef ACTIONS_H
#define ACTIONS_H

#include <Preferences.h>
#include <WiFi.h>
#include <globals.h>
#include <views.h>
#include <draw.h>


struct Action_Timer
{
    long screen;
    long probe;
    long wifi;
    long buzz;
};

void loop_action_wifi();
bool loop_action_display_probe(int &current_probe, int &display_state);
void loop_action_alarm(int &buzzer_beat, BUZZ_STATE &current_buzz);

#endif