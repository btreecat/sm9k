#ifndef STATES_H
#define STATES_H

enum DISPLAY_STATE
{
    HEAD,
    APIP,
    STAIP,
    PROBE,
    ALARM,
    TAIL
};

enum BUZZ_STATE
{
    BUZZ_ON,
    BUZZ_OFF,
};

#endif