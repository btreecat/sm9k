#include <actions.h>


int next_screen(int current_screen)
{

    if (current_screen == (TAIL - 1))
    {
        current_screen = HEAD + 1;
    }
    else
    {
        current_screen++;
    }
    return current_screen;
}

void sta_connect()
{
    // Serial.println("STA Connect Called");

    NV_STORAGE.begin(CFG_PREF_KEY, false);
    if (strcmp(NV_STORAGE.getString("sta_ssid", "").c_str(), "") != 0)
    {
        // WiFi.mode(WIFI_MODE_APSTA);
        //There is an SSID stored
        if (strcmp(NV_STORAGE.getString("sta_key", "").c_str(), "") != 0)
        {
            //there is a key stored
            WiFi.begin(NV_STORAGE.getString("sta_ssid").c_str(), NV_STORAGE.getString("sta_key").c_str());
        }
        else
        {
            WiFi.begin(NV_STORAGE.getString("sta_ssid").c_str());
        }
        WiFi.setAutoReconnect(true);
    }
    else
    {
        //No SSID set, set mode back to AP
        // WiFi.mode(WIFI_MODE_AP);
    }
    NV_STORAGE.end();
}

void loop_action_wifi()
{
    if (!WiFi.isConnected())
    {
        sta_connect();
    }
    else
    {
        NV_STORAGE.begin(CFG_PREF_KEY, false);
        if (strcmp(NV_STORAGE.getString(CFG_KEY_STA_SSID, "").c_str(), WiFi.SSID().c_str()) != 0 || strcmp(NV_STORAGE.getString(CFG_KEY_STA_KEY, "").c_str(), WiFi.psk().c_str()) != 0)
        {
            //Stored sta_ssid and connected sta_ssid differ
            NV_STORAGE.end();
            // WiFi.disconnect();
            // WiFi.mode(WIFI_MODE_AP);
        }
    }
}

bool loop_action_display_probe(int &current_probe, int &display_state)
{
    bool skip = false;
    if (current_probe < CFG_PROBE_PORTS)
    {
        //update the screen with the current probe
        //Send the main temp, the secondary temp, and matching units to the draw function
        if (PROBE_READINGS[current_probe].connected)
        {
            //Current ADC reading means there is probably a probe attached
            NV_STORAGE.begin(CFG_PREF_KEY, false);
            draw_temps(PROBE_READINGS[current_probe].temp_C, PROBE_READINGS[current_probe].temp_F, current_probe);
            NV_STORAGE.end();
        }
        else
        {
            skip = true;
        }
        current_probe++;
    }
    else
    {
        //reset current probe and change display state
        current_probe = 0;
        display_state = next_screen(display_state);
        skip = true;
    }

    return skip;
}

void loop_action_alarm(int &buzzer_beat, BUZZ_STATE &current_buzz)
{
    // NV_STORAGE.begin(CFG_PREF_KEY, false);
    // char buffer[CFG_PROBE_PORTS * sizeof(Alarm)];
    // Alarm *alarm_set;

    // NV_STORAGE.getBytes(CFG_KEY_ALARM, buffer, CFG_PROBE_PORTS * sizeof(Alarm));
    // alarm_set = (Alarm *)buffer;
    // NV_STORAGE.end();

    bool alarm_trigger = false;

    for (int probe = 0; probe < CFG_PROBE_PORTS; probe++)
    {
        if (PROBE_READINGS[probe].connected)
        {
            if (ALARM_SETTINGS[probe].type == ALARM_GREATER_THAN)
            {
                if (PROBE_READINGS[probe].temp_C > ALARM_SETTINGS[probe].temp_C)
                {
                    alarm_trigger = true;
                }
            }
            else if (ALARM_SETTINGS[probe].type == ALARM_LESS_THAN)
            {
                if (PROBE_READINGS[probe].temp_C < ALARM_SETTINGS[probe].temp_C)
                {
                    alarm_trigger = true;
                }
            }
            else if (ALARM_SETTINGS[probe].type == ALARM_RANGE)
            {
                if (PROBE_READINGS[probe].temp_C < ALARM_SETTINGS[probe].temp_C - ALARM_SETTINGS[probe].range || PROBE_READINGS[probe].temp_C > ALARM_SETTINGS[probe].temp_C + ALARM_SETTINGS[probe].range)
                {
                    alarm_trigger = true;
                }
            }
        }
    }

    if (alarm_trigger)
    {
        if (buzzer_beat < CFG_BUZZ_BEATS)
        {
            if (CFG_BUZZ_PATTERN[buzzer_beat] == BUZZ_ON)
            {
                if (current_buzz != BUZZ_ON)
                {
                    // Serial.println("Buzzer ON");
                    digitalWrite(GPIO_NUM_16, HIGH);
                    current_buzz = BUZZ_ON;
                }
            }
            else
            {
                if (current_buzz != BUZZ_OFF)
                {
                    // Serial.println("Buzzer OFF");
                    digitalWrite(GPIO_NUM_16, LOW);
                    current_buzz = BUZZ_OFF;
                }
            }
            buzzer_beat++;
        }
        else
        {
            buzzer_beat = 0;
        }
    }
}