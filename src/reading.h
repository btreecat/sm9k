#ifndef READING_H
#define READING_H

struct Reading
{
    int adc;
    float volt;
    int resist;
    float temp_K;
    float temp_C;
    float temp_F;
    bool connected;
};

#endif