#include <views.h>



static int template_str_size = 512;

const int ALARM_TYPE_COUNT = 4;
const int ALARM_RANGE_COUNT = 6;

static String ALARM_TYPE_OPTIONS[ALARM_TYPE_COUNT] = {"Disabled", "Greater-Than", "Less-Than", "Range"};
static int ALARM_RANGE_OPTIONS[ALARM_RANGE_COUNT] = {0, 5, 10, 15, 20, 25};

String view_root(bool temp_unit_F)
{
    char template_str[template_str_size];
    String response = html_doc_open;
    response += html_head_open;
    response += html_head_meta_refresh;
    response += html_head_title;
    response += html_body_open;
    response += html_nav_root;
    if (temp_unit_F)
    {
        sprintf(template_str, html_table_head_temps, "F", "C");
    }
    else
    {
        sprintf(template_str, html_table_head_temps, "C", "F");
    }
    response += template_str;
    for (int probe = 0; probe < CFG_PROBE_PORTS; probe++)
    {
        if (temp_unit_F)
        {
            sprintf(template_str, html_table_row_temp, probe, PROBE_READINGS[probe].temp_F, PROBE_READINGS[probe].temp_C);
        }
        else
        {
            sprintf(template_str, html_table_row_temp, probe, PROBE_READINGS[probe].temp_C, PROBE_READINGS[probe].temp_F);
        }
        response += template_str;
    }
    response += html_table_close;
    response += html_body_close;
    response += html_doc_close;
    return response;
}

String generate_alarm_type(ALARM_TYPE alarm_type)
{
    String response = "";
    char template_str[template_str_size];
    for (int option = 0; option < ALARM_TYPE_COUNT; option++)
    {
        if (option == alarm_type)
        {
            sprintf(template_str, html_table_row_alarm_option, option, "selected", ALARM_TYPE_OPTIONS[option].c_str());
        }
        else
        {
            sprintf(template_str, html_table_row_alarm_option, option, "", ALARM_TYPE_OPTIONS[option].c_str());
        }
        response += template_str;
    }
    return response;
}

String generate_alarm_temp(int probe, bool temp_unit_F, Alarm *alarm_set)
{
    char template_str[template_str_size];
    if (temp_unit_F)
    {
        if (alarm_set[probe].temp_F != 0)
        {
            sprintf(template_str, html_table_row_alarm_temp, CFG_MIN_TEMP_F, CFG_MAX_TEMP_F, probe, "F", CFG_MIN_TEMP_F, CFG_MAX_TEMP_F, String(alarm_set[probe].temp_F));
        }
        else
        {
            sprintf(template_str, html_table_row_alarm_temp, CFG_MIN_TEMP_F, CFG_MAX_TEMP_F, probe, "F", CFG_MIN_TEMP_F, CFG_MAX_TEMP_F, "");
        }
    }
    else
    {
        if (alarm_set[probe].temp_C != 0)
        {
            sprintf(template_str, html_table_row_alarm_temp, CFG_MIN_TEMP_C, CFG_MAX_TEMP_C, probe, "C", CFG_MIN_TEMP_C, CFG_MAX_TEMP_C, String(alarm_set[probe].temp_C));
        }
        else
        {
            sprintf(template_str, html_table_row_alarm_temp, CFG_MIN_TEMP_C, CFG_MAX_TEMP_C, probe, "C", CFG_MIN_TEMP_C, CFG_MAX_TEMP_C, "");
        }
    }

    return String(template_str);
}

String generate_alarm_range(int probe, Alarm *alarm_set)
{
    String response = "";
    char template_str[template_str_size];
    for (int range = 0; range < ALARM_RANGE_COUNT; range++)
    {
        if (ALARM_RANGE_OPTIONS[range] == alarm_set[probe].range)
        {
            sprintf(template_str, html_table_row_alarm_option, ALARM_RANGE_OPTIONS[range], "selected", String(ALARM_RANGE_OPTIONS[range]));
        }
        else
        {
            sprintf(template_str, html_table_row_alarm_option, ALARM_RANGE_OPTIONS[range], "", String(ALARM_RANGE_OPTIONS[range]));
        }
        response += template_str;
    }
    return response;
}

String generate_alarm_form(bool temp_unit_F, Alarm *alarm_set)
{
    String response = "";
    char template_str[template_str_size];
    for (int probe = 0; probe < CFG_PROBE_PORTS; probe++)
    {
        sprintf(template_str, html_table_row_alarm_open, probe, probe);
        response += template_str;
        sprintf(template_str, html_table_row_alarm_select_open, "type", probe);
        response += template_str;
        response += generate_alarm_type(alarm_set[probe].type);
        sprintf(template_str, html_table_row_alarm_select_close);
        response += template_str;
        //alarm temp goes here
        response += generate_alarm_temp(probe, temp_unit_F, alarm_set);

        sprintf(template_str, html_table_row_alarm_select_open, "range", probe);
        response += template_str;

        //add range string
        response += generate_alarm_range(probe, alarm_set);

        sprintf(template_str, html_table_row_alarm_close, probe);
        response += template_str;
    }
    return response;
}

String view_alarms(bool temp_unit_F, Alarm *alarm_set)
{
    char template_str[template_str_size];
    String response = html_doc_open;
    response += html_head_open;
    response += html_head_meta_static;
    response += html_head_title;
    response += html_head_close;
    response += html_body_open;
    response += html_nav_alarm;
    response += html_form_alarm_open;
    if (temp_unit_F)
    {
        sprintf(template_str, html_table_head_alarms, "F");
    }
    else
    {
        sprintf(template_str, html_table_head_alarms, "C");
    }
    response += template_str;

    response += generate_alarm_form(temp_unit_F, alarm_set);

    response += html_table_close;
    response += html_form_close;
    response += html_body_close;
    response += html_doc_close;
    return response;
}

String view_cfg(bool temp_unit_F, String sta_ssid, String sta_key)
{
    String response = html_doc_open;
    char template_str[template_str_size];
    response += html_head_open;
    response += html_head_meta_static;
    response += html_head_title;
    response += html_body_open;
    response += html_nav_cfg;
    response += html_form_cfg_open;
    if (temp_unit_F)
    {
        sprintf(template_str, html_form_chk, "checked");
    }
    else
    {
        sprintf(template_str, html_form_chk, "");
    }
    response += template_str;
    sprintf(template_str, html_form_sta_ssid, sta_ssid.c_str());
    response += template_str;
    sprintf(template_str, html_form_sta_key, sta_key.c_str());
    response += template_str;
    response += html_table_close;
    response += html_form_close;
    response += html_body_close;
    response += html_doc_close;
    return response;
}

String view_api()
{
    String response = "[";
    for (int probe = 0; probe < CFG_PROBE_PORTS; probe++)
    {
        response += "{\"probe_id\": ";
        response += probe;
        response += ", \"temp_c\": ";
        response += PROBE_READINGS[probe].temp_C;
        response += ", \"temp_f\": ";
        response += PROBE_READINGS[probe].temp_F;
        response += ", \"adc\": ";
        response += PROBE_READINGS[probe].adc;
        response += "}";
        if (probe < CFG_PROBE_PORTS - 1)
        {
            response += ",";
        }
    }
    response += "]";
    return response;
}

String view_404(String uri, HTTPMethod method)
{
    String response = "File Not Found\n\n";
    response += "URI: ";
    response += uri;
    response += "\nMethod: ";
    response += (method == HTTP_GET) ? "GET" : "POST";
    response += "\nArguments: ";
    return response;
}
