#ifndef ALARM_H
#define ALARM_H

enum ALARM_TYPE
{
    ALARM_DISABLED,
    ALARM_GREATER_THAN,
    ALARM_LESS_THAN,
    ALARM_RANGE
};


struct Alarm
{
    ALARM_TYPE type;
    int range;
    int temp_C;
    int temp_F;
};



#endif