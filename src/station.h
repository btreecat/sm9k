#ifndef STATION_H
#define STATION_H

const int STR_MAX_LENGTH = 32;

struct WiFi_Station {
    char *SSID[STR_MAX_LENGTH];
    bool encrypted;
};

#endif