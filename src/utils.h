#ifndef UTILS_H
#define UTILS_H
#include <Arduino.h>
#include <globals.h>
#include <math.h>
#include <states.h>
#include <actions.h>
#include <WiFi.h>

extern esp_adc_cal_characteristics_t *ESP_ADC_CHARS;

float calc_voltage(int adc_value);
float calc_resistance(int r2, double v_in, float v_out);
float calc_temp_k(float R1, double Ash, double Bsh, double Csh);
float k_to_c(float temp_K);
float c_to_f(float temp_C);
float f_to_c(float temp_F);
int next_screen(int current_screen);
void read_probes(int probe_values[], int probe_ports, int probe_samples, adc1_channel_t probes[]);
void get_reading(int probe_ports, int probe_samples, adc1_channel_t probes[], int r2, float v_in, double ash, double bsh, double csh);
Action_Timer calc_timer_diffs(long time_current, Action_Timer timer_set);
void temp_unit_init();
void alarm_storage_init();
void scan_wifi();

#endif