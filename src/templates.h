#ifndef TEMPLATES_H
#define TEMPLATES_H

//HTTP Response Header
static const char *http_status = "HTTP/1.1 200 OK";
static const char *http_content_type = "Content-type: text/html; charset=utf-8";
static const char *http_server_name = "Server: arduino";

//HTTP Response Payload
static const char *html_doc_open = "<!DOCTYPE html><html>";

static const char *html_head_open = "<head>";
static const char *html_head_close = "</head>";
static const char *html_head_title = "<title>Smoke Master 9K</title>";
static const char *html_head_meta_refresh = "<meta http-equiv='refresh' content='5' charset='utf-8'/>";
static const char *html_head_meta_static = "<meta charset='utf-8'/>";

static const char *html_body_open = "<body>";
static const char *html_body_close = "</body>";

static const char *html_table_head_temps = "<table>"
                                           "<thead>"
                                           "<tr>"
                                           "<th>Probe ID</th>"
                                           "<th>Temp %s</th>"
                                           "<th>Temp %s</th>"
                                           "<tr>"
                                           "</thead>"
                                           "<tbody>";

static const char *html_table_head_alarms = "<table>"
                                            "<thead>"
                                            "<tr>"
                                            "<th>Probe ID</th>"
                                            "<th>Alarm Type</th>"
                                            "<th>Alarm Temp (%s)</th>"
                                            "<th>Temp Range</th>"
                                            "<tr>"
                                            "</thead>"
                                            "<tbody>";

static const char *html_table_row_temp = "<tr>"
                                         "<td>Probe %d</td>"
                                         "<td>%.1f C</td>"
                                         "<td>%.1f F</td>"
                                         "</tr>";

static const char *html_table_row_alarm_open = "<tr>"
                                               "<td>Probe %d</td>";

static const char *html_table_row_alarm_select_open = "<td><select name='alarm_%s_%d'>";

static const char *html_table_row_alarm_option = "<option value='%d' %s> %s </option>";

static const char *html_table_row_alarm_select_close = "</select></td>";

static const char *html_table_row_alarm_temp = "<td><input type='number' min='%d' max='%d' name='alarm_temp_%d' placeholder='Temp in %s [%d-%d]' value='%s'></td>";

static const char *html_table_row_alarm_close = "</tr>";

static const char *html_nav_root = "Temps | <a href='/alarm'>Alarms</a> | <a href='/cfg'>Settings</a><br/><hr/>";
static const char *html_nav_cfg = "<a href='/'>Temps</a> | <a href='/alarm'>Alarms</a> | Settings<br/><hr/>";
static const char *html_nav_alarm = "<a href='/'>Temps</a> | Alarms | <a href='/cfg'>Settings</a><br/><hr/>";

static const char *html_form_cfg_open = "<form action='/cfg' method='POST'>"
                                        "<table><thead><tr></tr></thead><tbody>";

static const char *html_form_alarm_open = "<form action='/alarm' method='POST'>"
                                          "<table><thead><tr></tr></thead><tbody>";

static const char *html_table_close = "</tbody></table>";                                          

static const char *html_form_close = "<input type='submit' value='Save' />"
                                     "</form>";

static const char *html_form_chk = "<tr><td>Prefer Fahrenheit:</td>"
                                   "<td><input type='checkbox' name='temp_unit_F' %s></td></tr>";

static const char *html_form_sta_ssid = "<tr><td>WiFi Network:</td>"
                                        "<td><input type='text' name='sta_ssid' placeholder='Your_WiFi' value='%s'></td></tr>";

static const char *html_form_sta_key = "<tr><td>WiFi Password:</td>"
                                       "<td><input type='password' name='sta_key' value='%s'></td></tr>";

static const char *html_doc_close = "</html>";

#endif