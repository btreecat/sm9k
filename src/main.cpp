#include <Arduino.h>
#include <globals.h>
#include <draw.h>
#include <utils.h>
#include <routes.h>
#include <actions.h>
#include <WiFi.h>


//GLOBALS
Alarm ALARM_SETTINGS[CFG_PROBE_PORTS];
Preferences NV_STORAGE;
Reading PROBE_READINGS[CFG_PROBE_PORTS];
bool TEMP_UNIT_F;

void setup()
{
    //Start the serial console for debugging. Can be disabled for production.
    Serial.begin(112500);
    U8G2_DISPLAY.begin();
    draw_boot();
    Serial.println("Booting Started");
    //Init the display and load the splash screen

    //Start a local AP for users to connect directly to Smoke Master 9k
    WiFi.mode(WIFI_MODE_APSTA);
    WiFi.softAP(CFG_AP_SSID);
    WiFi.setHostname(CFG_AP_HOSTNAME);

    //Configure ADC to get proper voltage reads
    adc1_config_width(ADC_WIDTH_BIT_12);
    for (int probe = 0; probe < CFG_PROBE_PORTS; probe++)
    {
        adc1_config_channel_atten(CFG_PROBES[probe], CFG_ADC_ATTEN);
    }
    ESP_ADC_CHARS = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_characterize(CFG_ADC_UNIT, CFG_ADC_ATTEN, ADC_WIDTH_BIT_12, CFG_VOLTAGE_REF, ESP_ADC_CHARS);

    //init storage
    alarm_storage_init();
    temp_unit_init();

    //Start the HTTP Server
    register_routes();
    http_server.begin();

    //Enable buzzer pin
    pinMode(GPIO_NUM_16, OUTPUT);

    // scan_wifi();

    Serial.println("Done Booting");
}

void loop()
{
    static Action_Timer timer_set = (Action_Timer){0, 0, 0, 0};
    static int display_state = HEAD + 1;
    static int current_probe = 0;
    static int buzzer_beat = 0;
    static BUZZ_STATE current_buzz = BUZZ_OFF;

    //http request handler
    http_server.handleClient();

    //Timer setup and computation. Must be done on every loop to check if we need to call an action
    long time_current = millis();
    Action_Timer diff_set = calc_timer_diffs(time_current, timer_set);

    //Action Checks
    if (diff_set.wifi >= CFG_LOOP_WIFI)
    {
        loop_action_wifi();
        // scan_wifi();
        timer_set.wifi = time_current;
    }

    if (diff_set.probe >= CFG_LOOP_PROBE)
    {
        get_reading(CFG_PROBE_PORTS, CFG_PROBE_SAMPLES, (adc1_channel_t *)CFG_PROBES, CFG_RESISTOR_2, CFG_VOLTAGE_IN, CFG_A_SH, CFG_B_SH, CFG_C_SH);

        timer_set.probe = time_current;
    }

    if (diff_set.screen >= CFG_LOOP_SCREEN)
    {
        //Skip frame delay flag
        bool skip = false;

        if (display_state == PROBE)
        {
            skip = loop_action_display_probe(current_probe, display_state);
        }
        else
        {
            if (display_state == APIP)
            {
                draw_IP(WiFi.softAPIP().toString().c_str(), CFG_AP_SSID);
            }
            else if (display_state == STAIP)
            {
                if (WiFi.isConnected())
                {
                    draw_IP(WiFi.localIP().toString().c_str(), WiFi.SSID().c_str());
                }
                else
                {
                    skip = true;
                }
            }
            else if (display_state == ALARM)
            {
                // Serial.println("Display Alarm Setting");
                // draw_string("Alarm", U8G2_DISPLAY);
                skip = true;
            }
            else
            {
                //Somehow we got into a weird state, lets log it, and then lets advanced the time and state
                Serial.println("Unknown Dsiplay State");
            }
            display_state = next_screen(display_state);
        }

        if (skip)
        {
            // Serial.println("Skipping");
            timer_set.screen -= CFG_LOOP_SCREEN;
        }
        else
        {
            timer_set.screen = time_current;
        }
    }

    if (diff_set.buzz >= CFG_LOOP_BUZZ)
    {
        loop_action_alarm(buzzer_beat, current_buzz);
        timer_set.buzz = time_current;
    }
}