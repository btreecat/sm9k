#include <routes.h>

WebServer http_server(CFG_HTTP_PORT);


const char *type_text_html = "text/html; charset=utf-8";
const char *type_text_plain = "text/plain; charset=utf-8";
const char *type_app_json = "application/json; charset=utf-8";

void route_root_get()
{
    NV_STORAGE.begin(CFG_PREF_KEY, false);
    String response = view_root(NV_STORAGE.getBool(CFG_KEY_TEMP_PREF, false));
    NV_STORAGE.end();
    http_server.send(HTTP_OK, type_text_html, response);
}

void route_cfg_get()
{
    NV_STORAGE.begin(CFG_PREF_KEY, false);
    String response = view_cfg(TEMP_UNIT_F, NV_STORAGE.getString(CFG_KEY_STA_SSID, ""), NV_STORAGE.getString("sta_key", ""));
    // nv_storage.remove(KEY_ALARM); //For testing the alarm on clean boot
    NV_STORAGE.end();
    http_server.send(HTTP_OK, type_text_html, response);
}

void route_cfg_post()
{
    //recevie new settings
    NV_STORAGE.begin(CFG_PREF_KEY, false);
    if (http_server.hasArg(CFG_KEY_TEMP_PREF))
    {
        NV_STORAGE.putBool(CFG_KEY_TEMP_PREF, true);
        TEMP_UNIT_F = true;
    }
    else
    {
        NV_STORAGE.putBool(CFG_KEY_TEMP_PREF, false);
        TEMP_UNIT_F = false;
    }
    
    if (http_server.hasArg(CFG_KEY_STA_SSID))
    {
        NV_STORAGE.putString(CFG_KEY_STA_SSID, http_server.arg(CFG_KEY_STA_SSID));
    }
    else
    {
        NV_STORAGE.remove(CFG_KEY_STA_SSID);
    }
    if (http_server.hasArg(CFG_KEY_STA_KEY))
    {
        NV_STORAGE.putString(CFG_KEY_STA_KEY, http_server.arg(CFG_KEY_STA_KEY));
    }
    else
    {
        NV_STORAGE.remove(CFG_KEY_STA_KEY);
    }
    NV_STORAGE.end();
    route_cfg_get();
}

void handleNotFound()
{
    String response = view_404(http_server.uri(), http_server.method());
    response += http_server.args();
    response += "\n";
    for (uint8_t i = 0; i < http_server.args(); i++)
    {
        response += " " + http_server.argName(i) + ": " + http_server.arg(i) + "\n";
    }
    http_server.send(HTTP_NOT_FOUND, type_text_plain, response);
}

void route_api_values()
{
    String response = view_api();
    http_server.send(HTTP_OK, type_app_json, response);
}

void route_alarm_get()
{
    // NV_STORAGE.begin(CFG_PREF_KEY, false);
    // char buffer[CFG_PROBE_PORTS * sizeof(Alarm)];
    // Alarm *alarm_set;

    // NV_STORAGE.getBytes(CFG_KEY_ALARM, buffer, CFG_PROBE_PORTS * sizeof(Alarm));
    // alarm_set = (Alarm *)buffer;
    String response = view_alarms(TEMP_UNIT_F, ALARM_SETTINGS);

    // NV_STORAGE.end();
    http_server.send(HTTP_OK, type_text_html, response);
}

void route_alarm_post()
{
    NV_STORAGE.begin(CFG_PREF_KEY, false);
    const char *arg_type = "alarm_type_%d";
    const char *arg_temp = "alarm_temp_%d";
    const char *arg_range = "alarm_range_%d";
    // Alarm alarm_set[CFG_PROBE_PORTS];

    for (int probe = 0; probe < CFG_PROBE_PORTS; probe++)
    {
        // Alarm alarm;
        char arg_str[16];
        sprintf(arg_str, arg_type, probe);
        ALARM_SETTINGS[probe].type = (ALARM_TYPE)http_server.arg(arg_str).toInt();
        sprintf(arg_str, arg_range, probe);
        ALARM_SETTINGS[probe].range = http_server.arg(arg_str).toInt();
        
        sprintf(arg_str, arg_temp, probe);
        if (TEMP_UNIT_F)
        {
            ALARM_SETTINGS[probe].temp_F = http_server.arg(arg_str).toInt();
            ALARM_SETTINGS[probe].temp_C = f_to_c(ALARM_SETTINGS[probe].temp_F);
        }
        else
        {
            ALARM_SETTINGS[probe].temp_C = http_server.arg(arg_str).toInt();
            ALARM_SETTINGS[probe].temp_F = c_to_f(ALARM_SETTINGS[probe].temp_C);
        }

    }
    NV_STORAGE.putBytes(CFG_KEY_ALARM, (void *)ALARM_SETTINGS, CFG_PROBE_PORTS * sizeof(Alarm));
    NV_STORAGE.end();
    route_alarm_get();
}

void register_routes()
{
    http_server.on("/alarm", HTTP_POST, route_alarm_post);
    http_server.on("/alarm", HTTP_GET, route_alarm_get);
    http_server.on("/cfg", HTTP_POST, route_cfg_post);
    http_server.on("/cfg", HTTP_GET, route_cfg_get);
    http_server.on("/", route_root_get);
    http_server.onNotFound(handleNotFound);
    http_server.on("/api", route_api_values);
}
