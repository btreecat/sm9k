#ifndef CONFIG_H
#define CONFIG_H

#include <driver/adc.h>
#include <states.h>


//Network Config Values
static const char *CFG_AP_HOSTNAME = "sm9k";
static const char *CFG_PREF_KEY = "bbq";
static const char *CFG_AP_SSID = "SmokeMaster";
static const int CFG_HTTP_PORT = 80;

//LOOP vars are in milliseconds
static const int CFG_LOOP_SCREEN = 4000;
static const int CFG_LOOP_PROBE = 1000;
static const int CFG_LOOP_WIFI = 30000;
static const int CFG_LOOP_BUZZ = 107;

//These values are probe dependent
static const double CFG_A_SH = 0.00073431401;
static const double CFG_B_SH = 0.00021574370;
static const double CFG_C_SH = 0.000000095156860;
static const int CFG_MIN_TEMP_C = 10;
static const int CFG_MAX_TEMP_C = 200;
static const int CFG_MIN_TEMP_F = 50;
static const int CFG_MAX_TEMP_F = 390;

//This value is ESP32 dependent
static const int CFG_RESISTOR_2 = 10000;
static const int CFG_PROBE_SAMPLES = 42;
static const float CFG_VOLTAGE_IN = 3.3;
static const int CFG_VOLTAGE_REF = 1139;
static const int CFG_PROBE_PORTS = 2;
static const adc1_channel_t CFG_PROBES[CFG_PROBE_PORTS] = {ADC1_CHANNEL_3, ADC1_CHANNEL_0};
static const adc_atten_t CFG_ADC_ATTEN = ADC_ATTEN_DB_11;
static const adc_unit_t CFG_ADC_UNIT = ADC_UNIT_1;
static const int CFG_MAX_SSID = 32;

//Prefrences Keys
static const char *CFG_KEY_ALARM = "alarm_set";
static const char *CFG_KEY_TEMP_PREF = "temp_unit_F";
static const char *CFG_KEY_STA_SSID = "sta_ssid";
static const char *CFG_KEY_STA_KEY = "sta_key";

//Buzzer Pattern
static const int CFG_BUZZ_BEATS = 20;
static const BUZZ_STATE CFG_BUZZ_PATTERN[CFG_BUZZ_BEATS] = {BUZZ_ON, BUZZ_OFF, BUZZ_OFF, BUZZ_ON, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF, BUZZ_OFF};

#endif