#include <utils.h>
esp_adc_cal_characteristics_t *ESP_ADC_CHARS;

float calc_voltage(int adc_value)
{
    uint32_t mvolt = esp_adc_cal_raw_to_voltage(adc_value, ESP_ADC_CHARS);
    return float(mvolt) / 1000.0;
}

float calc_resistance(int r2, double v_in, float v_out)
{
    float resist = ((r2 * v_in) / v_out) - r2;
    return resist;
}

float calc_temp_k(float R1, double Ash, double Bsh, double Csh)
{
    float k = 1 / (Ash + (Bsh * log(R1)) + (Csh * pow(log(R1), 3)));
    return k;
}

float k_to_c(float temp_K)
{
    return (temp_K - 273.15);
}

float c_to_f(float temp_C)
{
    return (temp_C * 9 / 5) + 32;
}

float f_to_c(float temp_F)
{
    return (temp_F - 32) * (5 / 9);
}

void read_probes(int probe_values[], int probe_ports, int probe_samples, adc1_channel_t probes[])
{
    for (int probe = 0; probe < probe_ports; probe++)
    {
        probe_values[probe] = 0;
        for (int sample = 0; sample < probe_samples; sample++)
        {
            probe_values[probe] += adc1_get_raw(probes[probe]);
        }
        probe_values[probe] = probe_values[probe] / probe_samples;
    }
}

void get_reading(int probe_ports, int probe_samples, adc1_channel_t probes[], int r2, float v_in, double ash, double bsh, double csh)
{
    int probe_values[probe_ports];
    read_probes(probe_values, probe_ports, probe_samples, probes);

    for (int probe = 0; probe < probe_ports; probe++)
    {

        PROBE_READINGS[probe].adc = probe_values[probe];
        PROBE_READINGS[probe].volt = calc_voltage(PROBE_READINGS[probe].adc);
        PROBE_READINGS[probe].resist = calc_resistance(r2, v_in, PROBE_READINGS[probe].volt);
        PROBE_READINGS[probe].temp_K = calc_temp_k(PROBE_READINGS[probe].resist, ash, bsh, csh);
        PROBE_READINGS[probe].temp_C = k_to_c(PROBE_READINGS[probe].temp_K);
        PROBE_READINGS[probe].temp_F = c_to_f(PROBE_READINGS[probe].temp_C);
        PROBE_READINGS[probe].connected = (PROBE_READINGS[probe].adc ? true : false);
    }
}

Action_Timer calc_timer_diffs(long time_current, Action_Timer timer_set)
{
    Action_Timer diff_set;
    diff_set.buzz = time_current - timer_set.buzz;
    diff_set.probe = time_current - timer_set.probe;
    diff_set.screen = time_current - timer_set.screen;
    diff_set.wifi = time_current - timer_set.wifi;
    return diff_set;
}

void temp_unit_init() {
    NV_STORAGE.begin(CFG_PREF_KEY, false);
    TEMP_UNIT_F = NV_STORAGE.getBool(CFG_KEY_TEMP_PREF, "false");
    NV_STORAGE.end();
}

void alarm_storage_init()
{
    //init storage for alarms
    NV_STORAGE.begin(CFG_PREF_KEY, false);
    size_t alarm_chk = NV_STORAGE.getBytesLength(CFG_KEY_ALARM);
    if (alarm_chk == 0)
    {
        Serial.println("Alarm Settings Empty");

        for (int probe = 0; probe < CFG_PROBE_PORTS; probe++)
        {
            ALARM_SETTINGS[probe].type = ALARM_DISABLED;
            ALARM_SETTINGS[probe].range = 0;
            ALARM_SETTINGS[probe].temp_F = 0;
            ALARM_SETTINGS[probe].temp_C = 0;
        }
    }
    else
    {
        Serial.println("Alarm Settings found");
        //Lets just set the alarm types to disabled
        char buffer[CFG_PROBE_PORTS * sizeof(Alarm)];
        NV_STORAGE.getBytes(CFG_KEY_ALARM, buffer, CFG_PROBE_PORTS * sizeof(Alarm));
        // Serial.println("storage read");
        Alarm *alarm_set;
        alarm_set = (Alarm *)buffer;

        for (int probe = 0; probe < CFG_PROBE_PORTS; probe++)
        {
            ALARM_SETTINGS[probe].type = ALARM_DISABLED;
            ALARM_SETTINGS[probe].range = alarm_set[probe].range;
            ALARM_SETTINGS[probe].temp_F = alarm_set[probe].temp_F;
            ALARM_SETTINGS[probe].temp_C = alarm_set[probe].temp_C;
        }
        NV_STORAGE.putBytes(CFG_KEY_ALARM, (void *)ALARM_SETTINGS, CFG_PROBE_PORTS * sizeof(Alarm));
    }

    NV_STORAGE.end();
}

void scan_wifi(int &unique_ssid, String ssid_set[])
{
    //Scan
    //Currently there seems to be a bug where this can only be called once.
    int numSsid = WiFi.scanNetworks(false, true, true);
    unique_ssid = 0;
    if (WiFi.scanComplete())
    {
        // Serial.printf("SSID Count: %d\n", WiFi.scanComplete());
        for (int thisNet = 0; thisNet < numSsid; thisNet++)
        {
            bool add = true;

            for (int ssid = 0; ssid < numSsid; ssid++)
            {
                if (ssid_set[ssid].equals(WiFi.SSID(thisNet)))
                {
                    add = false;
                }
            }

            if (add)
            {
                ssid_set[unique_ssid] = WiFi.SSID(thisNet);
                unique_ssid++;
            }

            // Serial.print(thisNet);
            // Serial.print(") ");
            // Serial.print(WiFi.SSID(thisNet));
            // Serial.print("\tSignal: ");
            // Serial.print(WiFi.RSSI(thisNet));
            // Serial.print(" dBm");
            // Serial.print("\tEncryption: ");
            // Serial.println((WiFi.encryptionType(thisNet) == WIFI_AUTH_OPEN) ? " " : "*");
        }
    }

    for (int i = 0; i < unique_ssid; i++)
    {
        Serial.println(ssid_set[i]);
    }
}